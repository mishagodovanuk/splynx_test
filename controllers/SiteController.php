<?php

namespace app\controllers;

use app\models\Players;
use app\models\Team;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */

    public function actionIndex()
    {
        $this->view->title='Football.yii';
        $model=Team::find()->all();
        return $this->render('index.twig',['model' => $model]);
    }

    public function actionCreateTeam()
    {
     $this->view->title='Create team';
        $model=new Team();
        if($model->load(Yii::$app->request->post())&&$model->validate()) {
            if($model->save()) {
                Yii::$app->session->setFlash('create_team_success');
            }
        }
        return $this->render('create_team.twig',['model' => $model]);
    }

    public function actionViewTeam($id)
    {
     $model=Team::findOne($id);
     $this->view->title=$model->name;
        return $this->render('team_view.twig',['model' => $model]);
    }

    public function actionPlayerCreate($id=null)
    {
        if($id!=null) {
            $this->view->title = 'Create player';
            $model = new Players();
            $team = Team::findOne($id);
            if ($model->load(Yii::$app->request->post())) {
                $model->team_id = $team->id;
                if ($model->save()) {
                    Yii::$app->session->setFlash('create_player_success');
                }
            }
            return $this->render('create_player.twig', [
                'model' => $model,
                'team' => $team
            ]);
        }
        else {
            $all_pl_create=1;
            $model= new Players();
            $teams=ArrayHelper::map(Team::find()->asArray()->all(),'id','name');
            if($model->load(Yii::$app->request->post())&&$model->validate())
            {
                if($model->save())
                {
                    Yii::$app->session->setFlash('create_player_success');
                }
            }
            return $this->render('create_player.twig',[
                'model' => $model,
                'teams' => $teams,
                'all_pl_create' => $all_pl_create
            ]);
        }
    }

    public function actionTeamUpdate($id)
    {
        $model=Team::findOne($id);
        $this->view->title=$model->name;
        if($model->load(Yii::$app->request->post())&&$model->validate()) {
            if($model->save()) {
                Yii::$app->session->setFlash('update_team_success');
            }
        }
        return $this->render('team_update.twig',['model' => $model]);
    }

    public function actionPlayerView($id)
    {
        $model=Players::findOne($id);
        $this->view->title=$model->surname;
        return $this->render('player_view.twig',['model' => $model]);
    }

    public function actionPlayerUpdate($id)
    {
        $model=Players::findOne($id);
        $this->view->title=$model->surname;
        $teams=ArrayHelper::map(Team::find()->asArray()->all(),'id','name');
        if($model->load(Yii::$app->request->post())&&$model->validate()) {
            if($model->save()) {
                Yii::$app->session->setFlash('player_update_success');
            }
        }
        return $this->render('player_update.twig',[
            'model' => $model,
            'teams' => $teams
        ]);
    }

    public function actionPlayerDelete($id)
    {
        $model=Players::findOne($id);
        if($model->delete()) {
            Yii::$app->session->setFlash('delete_player_success');
        }
        return $this->render('player_view.twig');
    }

    public function actionTeamDelete($id)
    {
        $model=Team::findOne($id);
        if($model->delete()) {
            if($players = Players::deleteAll(['team_id' => $id])) {
            }
             Yii::$app->session->setFlash('team_delete_success');
             return $this->render('player_view.twig');
        }
    }

    public function actionAllPlayers()
    {
        $model=Players::find()->all();
        return $this->render('all_players.twig',['model' => $model]);
    }
}
