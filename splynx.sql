-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Бер 02 2017 р., 20:29
-- Версія сервера: 5.5.53
-- Версія PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `splynx`
--
CREATE DATABASE IF NOT EXISTS `splynx` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `splynx`;

-- --------------------------------------------------------

--
-- Структура таблиці `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `birth_date` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `team_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `players`
--

INSERT INTO `players` (`id`, `name`, `surname`, `birth_date`, `position`, `team_id`) VALUES
(9, 'Lionel', 'Messi', '16-Mar-2017', 'attack', '2'),
(8, 'Andriy', 'Shevchenko', '01-Apr-1975', 'semi-defence', '4'),
(10, 'Carlos', 'Puyol', '13-Mar-2017', 'defence', '2'),
(7, 'Oleksander', 'Yarmolenko', '14-Aug-1979', 'attack', '4'),
(11, 'Xavi', 'Martinez', '13-Jun-1983', 'semi-defence', '2'),
(12, 'Cristiano', 'Ronaldo', '06-Jun-1985', 'attack', '3'),
(13, 'Mariano', 'Diaz', '13-Feb-1995', 'attack', '3'),
(14, 'Carim', 'Benzema', '18-Feb-1989', 'attack', '3'),
(15, 'Tonni', 'Cross', '15-May-1991', 'semi-defence', '3'),
(16, 'Luca', 'Modrich', '23-Feb-1983', 'semi-defence', '3'),
(17, 'Serhio', 'Ramos', '16-Jan-1985', 'defence', '3'),
(18, 'Pepe', 'pepe', '16-Jun-1979', 'defence', '3'),
(19, 'Iker', 'Casillas', '20-Aug-1979', 'goalkeeper', '3'),
(20, 'Luis', 'Suarez', '14-Sep-1988', 'attack', '2'),
(21, 'Carles', 'Alenya', '21-Jun-1998', 'semi-defence', '2'),
(22, 'Sergio', 'Busquets', '05-Oct-1988', 'semi-defence', '2'),
(23, 'Andre', 'Gomesh', '04-May-1994', 'semi-defence', '2'),
(24, 'Gerard', 'Rique', '04-Jul-1984', 'defence', '2'),
(25, 'Jordi', 'Alba', '06-Feb-1990', 'defence', '2'),
(26, 'Jordi', 'Masip', '18-May-1995', 'goalkeeper', '2'),
(27, 'Roman', 'Yarechuk', '19-Feb-1997', 'attack', '4'),
(28, 'Oleksander ', 'Gladkiy', '21-Apr-1988', 'attack', '4'),
(29, 'Denis', 'Harmash', '27-May-1990', 'semi-defence', '4'),
(30, 'Serhiy', 'Ribarka', '27-Aug-1990', 'semi-defence', '4'),
(31, 'Nikita', 'Burda', '07-Nov-1995', 'defence', '4'),
(32, 'Eugeniy', 'Khacheridy', '20-Jul-1988', 'defence', '4'),
(33, 'Nicola', 'Morozyuk', '19-Aug-1986', 'defence', '4'),
(34, 'Arthur', 'Rudko', '22-Jun-1989', 'goalkeeper', '4'),
(35, 'Oleksander', 'Zubkov', '12-Dec-1989', 'attack', '5'),
(36, 'Andriy', 'Boyarchuk', '24-Apr-1997', 'attack', '5'),
(37, 'Taras', 'Stepanenko', '25-Oct-1993', 'semi-defence', '5'),
(38, 'Maksim', 'Malishev', '11-Jun-1992', 'semi-defence', '5'),
(39, 'Vyacheslav', 'Tankovskiy', '09-Mar-1996', 'semi-defence', '5'),
(40, 'Ivan', 'Ordec', '01-Oct-1991', 'defence', '5'),
(41, 'Marsio', 'Azevedo', '20-Jul-1980', 'defence', '5'),
(42, 'Bogdan', 'Butko', '15-Jan-1991', 'defence', '5'),
(43, 'Andriy', 'Pyatov', '06-Jul-1978', 'goalkeeper', '5'),
(44, 'Danny', 'Engs', '18-Jul-1984', 'attack', '6'),
(45, 'Danielle', 'Starridge', '04-Jun-1990', 'attack', '6'),
(46, 'Sadyo', 'Mane', '10-Jul-1990', 'semi-defence', '6'),
(47, 'Marco', 'Gruyiich', '12-Jun-1995', 'semi-defence', '6'),
(48, 'Lucas', 'Leyva', '22-Jul-1994', 'semi-defence', '6'),
(49, 'Conor', 'McGregor', '04-Jul-1988', 'defence', '6'),
(50, 'Djosef', 'Homes', '25-Oct-1995', 'defence', '6'),
(51, 'Alberto', 'Moreno', '17-Jun-1985', 'defence', '6'),
(52, 'Loris', 'Calius', '12-Mar-1991', 'goalkeeper', '6'),
(53, 'Oleg', 'Senchov', '17-Oct-1979', 'goalkeeper', '4'),
(54, 'Vasil', 'Olegov', '03-Jul-1986', 'goalkeeper', '5');

-- --------------------------------------------------------

--
-- Структура таблиці `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `foundation_date` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `team`
--

INSERT INTO `team` (`id`, `name`, `foundation_date`) VALUES
(2, 'Barcelona', '17-Mar-2017'),
(3, 'Real Madrid', '26-Mar-2017'),
(4, 'Dynamo', '26-Mar-2017'),
(5, 'Shahtar', '18-Mar-2017'),
(6, 'Liverpool', '31-May-1924');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
