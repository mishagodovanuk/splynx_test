<?php
namespace app\models;

use yii\db\ActiveRecord;
use app\models\Team;

class Players extends ActiveRecord
{

    public static function tableName()
    {
        return 'players';
    }

    public function rules()
    {
        return[
            [['name','surname','birth_date','position','team_id'],'required']
        ];
    }

    public function getTeam()
    {
        return $this->hasOne(Team::className(),['id' => 'team_id']);
    }

    public function attributeLabels()
    {
        return [
            'id'=> 'id',
            'name'=>'name',
            'surname'=> 'surname',
            'birth_date'=> 'birth_date',
            'position'=> 'position',
            'team_id'=> 'team_id'
        ];
    }

}