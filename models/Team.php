<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.03.2017
 * Time: 18:29
 */

namespace app\models;

use yii\db\ActiveRecord;
use app\models\Players;
use yii\helpers\ArrayHelper;

class Team extends ActiveRecord
{

    public static function tableName()
    {
        return 'team';
    }

    public function rules()
    {
        return [
            [['name','foundation_date'],'required'],
        ];
    }

    public function getPlayers()
    {
        return $this->hasMany(Players::className(),['team_id' => 'id']);
    }
}