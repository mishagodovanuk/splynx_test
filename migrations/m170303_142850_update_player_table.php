<?php

use yii\db\Migration;

class m170303_142850_update_player_table extends Migration
{
    public function up()
    {
        $this->insert('players',[
            'id' => 9,
            'name' => 'Lionel',
            'surname' => 'Messi',
            'birth_date' => '16-Mar-2017',
            'position' => 'attack',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 8,
            'name' => 'Andriy',
            'surname' => 'Shevchenko',
            'birth_date' => '01-Apr-1975',
            'position' => 'semi-defence',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 10,
            'name' => 'Carlos',
            'surname' => 'Puyol',
            'birth_date' => '13-Mar-2017',
            'position' => 'defence',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 7,
            'name' => 'Oleksander',
            'surname' => 'Yarmolenko',
            'birth_date' => '14-Aug-1979',
            'position' => 'attack',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 11,
            'name' => 'Xavi',
            'surname' => 'Martinez',
            'birth_date' => '13-Jun-1983',
            'position' => 'semi-defence',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 12,
            'name' => 'Cristiano',
            'surname' => 'Ronaldo',
            'birth_date' => '06-Jun-1985',
            'position' => 'attack',
            'team_id' => '3'
        ]);
        $this->insert('players',[
            'id' => 13,
            'name' => 'Mariano',
            'surname' => 'Diaz',
            'birth_date' => '13-Feb-1995',
            'position' => 'attack',
            'team_id' => '3'
        ]);
        $this->insert('players',[
            'id' => 14,
            'name' => 'Carim',
            'surname' => 'Benzema',
            'birth_date' => '18-Feb-1989',
            'position' => 'attack',
            'team_id' => '3'
        ]);
        $this->insert('players',[
            'id' => 15,
            'name' => 'Tonni',
            'surname' => 'Cross',
            'birth_date' => '15-May-1991',
            'position' => 'semi-defence',
            'team_id' => '3'
        ]);
        $this->insert('players',[
            'id' => 16,
            'name' => 'Luca',
            'surname' => 'Modrich',
            'birth_date' => '23-Feb-1983',
            'position' => 'semi-defence',
            'team_id' => '3'
        ]);
        $this->insert('players',[
            'id' => 17,
            'name' => 'Serhio',
            'surname' => 'Ramos',
            'birth_date' => '16-Jan-1985',
            'position' => 'defence',
            'team_id' => '3'
        ]);
        $this->insert('players',[
            'id' => 18,
            'name' => 'Pepe',
            'surname' => 'pepe',
            'birth_date' => '16-Jun-1979',
            'position' => 'defence',
            'team_id' => '3'
        ]);
        $this->insert('players',[
            'id' => 19,
            'name' => 'Iker',
            'surname' => 'Casillas',
            'birth_date' => '20-Aug-1979',
            'position' => 'goalkeeper',
            'team_id' => '3'
        ]);
        $this->insert('players',[
            'id' => 20,
            'name' => 'Luis',
            'surname' => 'Suarez',
            'birth_date' => '14-Sep-1988',
            'position' => 'attack',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 21,
            'name' => 'Carles',
            'surname' => 'Alenya',
            'birth_date' => '21-Jun-1998',
            'position' => 'semi-defence',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 22,
            'name' => 'Sergio',
            'surname' => 'Busquets',
            'birth_date' => '05-Oct-1988',
            'position' => 'semi-defence',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 23,
            'name' => 'Andre',
            'surname' => 'Gomesh',
            'birth_date' => '04-May-1994',
            'position' => 'semi-defence',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 24,
            'name' => 'Gerard',
            'surname' => 'Rique',
            'birth_date' => '04-Jul-1984',
            'position' => 'defence',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 25,
            'name' => 'Jordi',
            'surname' => 'Masip',
            'birth_date' => '18-May-1995',
            'position' => 'goalkeeper',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 26,
            'name' => 'Jordi',
            'surname' => 'Alba',
            'birth_date' => '06-Feb-1990',
            'position' => 'defence',
            'team_id' => '2'
        ]);
        $this->insert('players',[
            'id' => 27,
            'name' => 'Roman',
            'surname' => 'Yarechuk',
            'birth_date' => '19-Feb-1997',
            'position' => 'attack',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 28,
            'name' => 'Oleksander',
            'surname' => 'Gladkiy',
            'birth_date' => '21-Apr-1988',
            'position' => 'attack',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 29,
            'name' => 'Denis',
            'surname' => 'Harmash',
            'birth_date' => '27-May-1990',
            'position' => 'semi-defence',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 30,
            'name' => 'Serhiy',
            'surname' => 'Ribarka',
            'birth_date' => '27-Aug-1990',
            'position' => 'semi-defence',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 31,
            'name' => 'Nikita',
            'surname' => 'Burda',
            'birth_date' => '07-Nov-1995',
            'position' => 'defence',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 32,
            'name' => 'Eugeniy',
            'surname' => 'Khacheridy',
            'birth_date' => '20-Jul-1988',
            'position' => 'defence',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 33,
            'name' => 'Nicola',
            'surname' => 'Morozyuk',
            'birth_date' => '19-Aug-1986',
            'position' => 'defence',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 34,
            'name' => 'Arthur',
            'surname' => 'Rudko',
            'birth_date' => '22-Jun-1989',
            'position' => 'goalkeeper',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 35,
            'name' => 'Oleksander',
            'surname' => 'Zubkov',
            'birth_date' => '12-Dec-1989',
            'position' => 'attack',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 36,
            'name' => 'Andriy',
            'surname' => 'Boyarchuk',
            'birth_date' => '24-Apr-1997',
            'position' => 'attack',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 37,
            'name' => 'Taras',
            'surname' => 'Stepanenko',
            'birth_date' => '25-Oct-1993',
            'position' => 'semi-defence',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 38,
            'name' => 'Maksim',
            'surname' => 'Malishev',
            'birth_date' => '11-Jun-1992',
            'position' => 'semi-defence',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 39,
            'name' => 'Vyacheslav',
            'surname' => 'Tankovskiy',
            'birth_date' => '09-Mar-1996',
            'position' => 'semi-defence',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 40,
            'name' => 'Ivan',
            'surname' => 'Ordec',
            'birth_date' => '01-Oct-1991',
            'position' => 'defence',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 41,
            'name' => 'Marsio',
            'surname' => 'Azevedo',
            'birth_date' => '20-Jul-1980',
            'position' => 'defence',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 42,
            'name' => 'Bogdan',
            'surname' => 'Butko',
            'birth_date' => '15-Jan-1991',
            'position' => 'defence',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 43,
            'name' => 'Andriy',
            'surname' => 'Pyatov',
            'birth_date' => '06-Jul-1978',
            'position' => 'goalkeeper',
            'team_id' => '5'
        ]);
        $this->insert('players',[
            'id' => 44,
            'name' => 'Danny',
            'surname' => 'Engs',
            'birth_date' => '18-Jul-1984',
            'position' => 'attack',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 45,
            'name' => 'Danielle',
            'surname' => 'Starridge',
            'birth_date' => '04-Jun-1990',
            'position' => 'attack',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 46,
            'name' => 'Sadyo',
            'surname' => 'Mane',
            'birth_date' => '10-Jul-1990',
            'position' => 'semi-defence',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 47,
            'name' => 'Marco',
            'surname' => 'Gruyiich',
            'birth_date' => '12-Jun-1995',
            'position' => 'semi-defence',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 48,
            'name' => 'Lucas',
            'surname' => 'Leyva',
            'birth_date' => '22-Jul-1994',
            'position' => 'semi-defence',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 49,
            'name' => 'Conor',
            'surname' => 'McGregor',
            'birth_date' => '04-Jul-1988',
            'position' => 'defence',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 50,
            'name' => 'Djosef',
            'surname' => 'Homes',
            'birth_date' => '25-Oct-1995',
            'position' => 'defence',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 51,
            'name' => 'Alberto',
            'surname' => 'Moreno',
            'birth_date' => '17-Jun-1985',
            'position' => 'defence',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 52,
            'name' => 'Loris',
            'surname' => 'Calius',
            'birth_date' => '12-Mar-1991',
            'position' => 'goalkeeper',
            'team_id' => '6'
        ]);
        $this->insert('players',[
            'id' => 53,
            'name' => 'Oleg',
            'surname' => 'Senchov',
            'birth_date' => '17-Oct-1979',
            'position' => 'goalkeeper',
            'team_id' => '4'
        ]);
        $this->insert('players',[
            'id' => 54,
            'name' => 'Vasil',
            'surname' => 'Olegov',
            'birth_date' => '03-Jul-1986',
            'position' => 'goalkeeper',
            'team_id' => '5'
        ]);
    }


    public function down()
    {
        echo "m170303_142850_update_player_table cannot be reverted.\n";

        return false;
    }

}
