<?php

use yii\db\Migration;

class m170303_134847_update_team_table extends Migration
{
    public function up()
    {
        $this->insert('team',[
            'id' => 2,
            'name' => 'Barcelona',
            'foundation_date' => '17-Mar-2017'
        ]);

        $this->insert('team',[
            'id' => 3,
            'name' => 'Real Madrid',
            'foundation_date' => '26-Mar-2017'
        ]);

        $this->insert('team',[
            'id' => 4,
            'name' => 'Dynamo',
            'foundation_date' => '26-Mar-2017'
        ]);

        $this->insert('team',[
            'id' => 5,
            'name' => 'Shahtar',
            'foundation_date' => '18-Mar-2017'
        ]);

        $this->insert('team',[
            'id' => 6,
            'name' => 'Liverpool',
            'foundation_date' => '31-May-1924'
        ]);

    }


    public function down()
    {
        echo "m170303_134847_update_team_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */




}
