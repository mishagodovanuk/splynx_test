<?php

use yii\db\Migration;

/**
 * Handles the creation of table `player`.
 */
class m170303_142537_create_player_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('players', [
            'id' => $this->primaryKey()->notNull(),
            'name' => $this->char(255)->notNull(),
            'surname' => $this->char(255)->notNull(),
            'birth_date' => $this->char(255)->notNull(),
            'position' => $this->char(255)->notNull(),
            'team_id' => $this->char(255)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('player');
    }
}
