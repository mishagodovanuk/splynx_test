<?php

use yii\db\Migration;

/**
 * Handles the creation of table `team`.
 */
class m170303_134409_create_team_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('team', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->char(255)->notNull(),
            'foundation_date' => $this->char(255)->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('team');
    }
}
